package com.fhoster.jooqutils;

import org.jooq.Record;
import org.jooq.Table;
import org.jooq.TableField;

@FunctionalInterface
public interface TableFieldProvider<T extends Table<R>, R extends Record, O> {
	TableField<R, O> apply(T table);
}
