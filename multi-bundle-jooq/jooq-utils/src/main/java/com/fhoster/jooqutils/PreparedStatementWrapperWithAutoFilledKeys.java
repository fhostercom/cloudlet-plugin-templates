package com.fhoster.jooqutils;

import java.io.InputStream;
import java.io.Reader;
import java.math.BigDecimal;
import java.net.URL;
import java.sql.Array;
import java.sql.Blob;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.Date;
import java.sql.NClob;
import java.sql.ParameterMetaData;
import java.sql.PreparedStatement;
import java.sql.Ref;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.RowId;
import java.sql.SQLException;
import java.sql.SQLType;
import java.sql.SQLWarning;
import java.sql.SQLXML;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Calendar;

import org.jooq.exception.DataAccessException;

public class PreparedStatementWrapperWithAutoFilledKeys implements PreparedStatement {

	private PreparedStatement delegator;

	public PreparedStatementWrapperWithAutoFilledKeys(PreparedStatement delegator) {
		this.delegator = delegator;
	}
	
	public ResultSet getGeneratedKeys() throws SQLException {
		throw new DataAccessException("Attempt to invoke getGeneratedKeys() on a statement which has been injected"
				+ " with keys created by a jooq visitor. "
				+ "This happens when you use returning(), but in this case you have to inject id on your own.");
	}

	//***********************************************
	// remained method always delegate to delegator
	//***********************************************
	
	public <T> T unwrap(Class<T> iface) throws SQLException {
		return delegator.unwrap(iface);
	}

	public ResultSet executeQuery(String sql) throws SQLException {
		return delegator.executeQuery(sql);
	}

	public ResultSet executeQuery() throws SQLException {
		return delegator.executeQuery();
	}

	public boolean isWrapperFor(Class<?> iface) throws SQLException {
		return delegator.isWrapperFor(iface);
	}

	public int executeUpdate(String sql) throws SQLException {
		return delegator.executeUpdate(sql);
	}

	public int executeUpdate() throws SQLException {
		return delegator.executeUpdate();
	}

	public void setNull(int parameterIndex, int sqlType) throws SQLException {
		delegator.setNull(parameterIndex, sqlType);
	}

	public void close() throws SQLException {
		delegator.close();
	}

	public int getMaxFieldSize() throws SQLException {
		return delegator.getMaxFieldSize();
	}

	public void setBoolean(int parameterIndex, boolean x) throws SQLException {
		delegator.setBoolean(parameterIndex, x);
	}

	public void setByte(int parameterIndex, byte x) throws SQLException {
		delegator.setByte(parameterIndex, x);
	}

	public void setMaxFieldSize(int max) throws SQLException {
		delegator.setMaxFieldSize(max);
	}

	public void setShort(int parameterIndex, short x) throws SQLException {
		delegator.setShort(parameterIndex, x);
	}

	public int getMaxRows() throws SQLException {
		return delegator.getMaxRows();
	}

	public void setInt(int parameterIndex, int x) throws SQLException {
		delegator.setInt(parameterIndex, x);
	}

	public void setMaxRows(int max) throws SQLException {
		delegator.setMaxRows(max);
	}

	public void setLong(int parameterIndex, long x) throws SQLException {
		delegator.setLong(parameterIndex, x);
	}

	public void setFloat(int parameterIndex, float x) throws SQLException {
		delegator.setFloat(parameterIndex, x);
	}

	public void setEscapeProcessing(boolean enable) throws SQLException {
		delegator.setEscapeProcessing(enable);
	}

	public void setDouble(int parameterIndex, double x) throws SQLException {
		delegator.setDouble(parameterIndex, x);
	}

	public int getQueryTimeout() throws SQLException {
		return delegator.getQueryTimeout();
	}

	public void setBigDecimal(int parameterIndex, BigDecimal x) throws SQLException {
		delegator.setBigDecimal(parameterIndex, x);
	}

	public void setQueryTimeout(int seconds) throws SQLException {
		delegator.setQueryTimeout(seconds);
	}

	public void setString(int parameterIndex, String x) throws SQLException {
		delegator.setString(parameterIndex, x);
	}

	public void setBytes(int parameterIndex, byte[] x) throws SQLException {
		delegator.setBytes(parameterIndex, x);
	}

	public void cancel() throws SQLException {
		delegator.cancel();
	}

	public void setDate(int parameterIndex, Date x) throws SQLException {
		delegator.setDate(parameterIndex, x);
	}

	public SQLWarning getWarnings() throws SQLException {
		return delegator.getWarnings();
	}

	public void setTime(int parameterIndex, Time x) throws SQLException {
		delegator.setTime(parameterIndex, x);
	}

	public void setTimestamp(int parameterIndex, Timestamp x) throws SQLException {
		delegator.setTimestamp(parameterIndex, x);
	}

	public void clearWarnings() throws SQLException {
		delegator.clearWarnings();
	}

	public void setCursorName(String name) throws SQLException {
		delegator.setCursorName(name);
	}

	public void setAsciiStream(int parameterIndex, InputStream x, int length) throws SQLException {
		delegator.setAsciiStream(parameterIndex, x, length);
	}

	@SuppressWarnings("deprecation")
	public void setUnicodeStream(int parameterIndex, InputStream x, int length) throws SQLException {
		delegator.setUnicodeStream(parameterIndex, x, length);
	}

	public boolean execute(String sql) throws SQLException {
		return delegator.execute(sql);
	}

	public void setBinaryStream(int parameterIndex, InputStream x, int length) throws SQLException {
		delegator.setBinaryStream(parameterIndex, x, length);
	}

	public ResultSet getResultSet() throws SQLException {
		return delegator.getResultSet();
	}

	public int getUpdateCount() throws SQLException {
		return delegator.getUpdateCount();
	}

	public void clearParameters() throws SQLException {
		delegator.clearParameters();
	}

	public boolean getMoreResults() throws SQLException {
		return delegator.getMoreResults();
	}

	public void setObject(int parameterIndex, Object x, int targetSqlType) throws SQLException {
		delegator.setObject(parameterIndex, x, targetSqlType);
	}

	public void setFetchDirection(int direction) throws SQLException {
		delegator.setFetchDirection(direction);
	}

	public void setObject(int parameterIndex, Object x) throws SQLException {
		delegator.setObject(parameterIndex, x);
	}

	public int getFetchDirection() throws SQLException {
		return delegator.getFetchDirection();
	}

	public void setFetchSize(int rows) throws SQLException {
		delegator.setFetchSize(rows);
	}

	public int getFetchSize() throws SQLException {
		return delegator.getFetchSize();
	}

	public boolean execute() throws SQLException {
		return delegator.execute();
	}

	public int getResultSetConcurrency() throws SQLException {
		return delegator.getResultSetConcurrency();
	}

	public int getResultSetType() throws SQLException {
		return delegator.getResultSetType();
	}

	public void addBatch(String sql) throws SQLException {
		delegator.addBatch(sql);
	}

	public void addBatch() throws SQLException {
		delegator.addBatch();
	}

	public void setCharacterStream(int parameterIndex, Reader reader, int length) throws SQLException {
		delegator.setCharacterStream(parameterIndex, reader, length);
	}

	public void clearBatch() throws SQLException {
		delegator.clearBatch();
	}

	public int[] executeBatch() throws SQLException {
		return delegator.executeBatch();
	}

	public void setRef(int parameterIndex, Ref x) throws SQLException {
		delegator.setRef(parameterIndex, x);
	}

	public void setBlob(int parameterIndex, Blob x) throws SQLException {
		delegator.setBlob(parameterIndex, x);
	}

	public void setClob(int parameterIndex, Clob x) throws SQLException {
		delegator.setClob(parameterIndex, x);
	}

	public void setArray(int parameterIndex, Array x) throws SQLException {
		delegator.setArray(parameterIndex, x);
	}

	public Connection getConnection() throws SQLException {
		return delegator.getConnection();
	}

	public ResultSetMetaData getMetaData() throws SQLException {
		return delegator.getMetaData();
	}

	public void setDate(int parameterIndex, Date x, Calendar cal) throws SQLException {
		delegator.setDate(parameterIndex, x, cal);
	}

	public boolean getMoreResults(int current) throws SQLException {
		return delegator.getMoreResults(current);
	}

	public void setTime(int parameterIndex, Time x, Calendar cal) throws SQLException {
		delegator.setTime(parameterIndex, x, cal);
	}

	public void setTimestamp(int parameterIndex, Timestamp x, Calendar cal) throws SQLException {
		delegator.setTimestamp(parameterIndex, x, cal);
	}

	public int executeUpdate(String sql, int autoGeneratedKeys) throws SQLException {
		return delegator.executeUpdate(sql, autoGeneratedKeys);
	}

	public void setNull(int parameterIndex, int sqlType, String typeName) throws SQLException {
		delegator.setNull(parameterIndex, sqlType, typeName);
	}

	public int executeUpdate(String sql, int[] columnIndexes) throws SQLException {
		return delegator.executeUpdate(sql, columnIndexes);
	}

	public void setURL(int parameterIndex, URL x) throws SQLException {
		delegator.setURL(parameterIndex, x);
	}

	public ParameterMetaData getParameterMetaData() throws SQLException {
		return delegator.getParameterMetaData();
	}

	public void setRowId(int parameterIndex, RowId x) throws SQLException {
		delegator.setRowId(parameterIndex, x);
	}

	public int executeUpdate(String sql, String[] columnNames) throws SQLException {
		return delegator.executeUpdate(sql, columnNames);
	}

	public void setNString(int parameterIndex, String value) throws SQLException {
		delegator.setNString(parameterIndex, value);
	}

	public void setNCharacterStream(int parameterIndex, Reader value, long length) throws SQLException {
		delegator.setNCharacterStream(parameterIndex, value, length);
	}

	public boolean execute(String sql, int autoGeneratedKeys) throws SQLException {
		return delegator.execute(sql, autoGeneratedKeys);
	}

	public void setNClob(int parameterIndex, NClob value) throws SQLException {
		delegator.setNClob(parameterIndex, value);
	}

	public void setClob(int parameterIndex, Reader reader, long length) throws SQLException {
		delegator.setClob(parameterIndex, reader, length);
	}

	public boolean execute(String sql, int[] columnIndexes) throws SQLException {
		return delegator.execute(sql, columnIndexes);
	}

	public void setBlob(int parameterIndex, InputStream inputStream, long length) throws SQLException {
		delegator.setBlob(parameterIndex, inputStream, length);
	}

	public void setNClob(int parameterIndex, Reader reader, long length) throws SQLException {
		delegator.setNClob(parameterIndex, reader, length);
	}

	public boolean execute(String sql, String[] columnNames) throws SQLException {
		return delegator.execute(sql, columnNames);
	}

	public void setSQLXML(int parameterIndex, SQLXML xmlObject) throws SQLException {
		delegator.setSQLXML(parameterIndex, xmlObject);
	}

	public void setObject(int parameterIndex, Object x, int targetSqlType, int scaleOrLength) throws SQLException {
		delegator.setObject(parameterIndex, x, targetSqlType, scaleOrLength);
	}

	public int getResultSetHoldability() throws SQLException {
		return delegator.getResultSetHoldability();
	}

	public boolean isClosed() throws SQLException {
		return delegator.isClosed();
	}

	public void setPoolable(boolean poolable) throws SQLException {
		delegator.setPoolable(poolable);
	}

	public void setAsciiStream(int parameterIndex, InputStream x, long length) throws SQLException {
		delegator.setAsciiStream(parameterIndex, x, length);
	}

	public boolean isPoolable() throws SQLException {
		return delegator.isPoolable();
	}

	public void closeOnCompletion() throws SQLException {
		delegator.closeOnCompletion();
	}

	public void setBinaryStream(int parameterIndex, InputStream x, long length) throws SQLException {
		delegator.setBinaryStream(parameterIndex, x, length);
	}

	public boolean isCloseOnCompletion() throws SQLException {
		return delegator.isCloseOnCompletion();
	}

	public void setCharacterStream(int parameterIndex, Reader reader, long length) throws SQLException {
		delegator.setCharacterStream(parameterIndex, reader, length);
	}

	public long getLargeUpdateCount() throws SQLException {
		return delegator.getLargeUpdateCount();
	}

	public void setLargeMaxRows(long max) throws SQLException {
		delegator.setLargeMaxRows(max);
	}

	public void setAsciiStream(int parameterIndex, InputStream x) throws SQLException {
		delegator.setAsciiStream(parameterIndex, x);
	}

	public long getLargeMaxRows() throws SQLException {
		return delegator.getLargeMaxRows();
	}

	public void setBinaryStream(int parameterIndex, InputStream x) throws SQLException {
		delegator.setBinaryStream(parameterIndex, x);
	}

	public long[] executeLargeBatch() throws SQLException {
		return delegator.executeLargeBatch();
	}

	public void setCharacterStream(int parameterIndex, Reader reader) throws SQLException {
		delegator.setCharacterStream(parameterIndex, reader);
	}

	public void setNCharacterStream(int parameterIndex, Reader value) throws SQLException {
		delegator.setNCharacterStream(parameterIndex, value);
	}

	public long executeLargeUpdate(String sql) throws SQLException {
		return delegator.executeLargeUpdate(sql);
	}

	public void setClob(int parameterIndex, Reader reader) throws SQLException {
		delegator.setClob(parameterIndex, reader);
	}

	public long executeLargeUpdate(String sql, int autoGeneratedKeys) throws SQLException {
		return delegator.executeLargeUpdate(sql, autoGeneratedKeys);
	}

	public void setBlob(int parameterIndex, InputStream inputStream) throws SQLException {
		delegator.setBlob(parameterIndex, inputStream);
	}

	public void setNClob(int parameterIndex, Reader reader) throws SQLException {
		delegator.setNClob(parameterIndex, reader);
	}

	public long executeLargeUpdate(String sql, int[] columnIndexes) throws SQLException {
		return delegator.executeLargeUpdate(sql, columnIndexes);
	}

	public void setObject(int parameterIndex, Object x, SQLType targetSqlType, int scaleOrLength)
			throws SQLException {
		delegator.setObject(parameterIndex, x, targetSqlType, scaleOrLength);
	}

	public long executeLargeUpdate(String sql, String[] columnNames) throws SQLException {
		return delegator.executeLargeUpdate(sql, columnNames);
	}

	public void setObject(int parameterIndex, Object x, SQLType targetSqlType) throws SQLException {
		delegator.setObject(parameterIndex, x, targetSqlType);
	}

	public long executeLargeUpdate() throws SQLException {
		return delegator.executeLargeUpdate();
	}
}
