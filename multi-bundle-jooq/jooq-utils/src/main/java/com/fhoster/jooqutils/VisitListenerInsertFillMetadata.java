package com.fhoster.jooqutils;

import java.lang.reflect.Method;
import java.sql.Timestamp;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jooq.Clause;
import org.jooq.Field;
import org.jooq.InsertQuery;
import org.jooq.QueryPart;
import org.jooq.Table;
import org.jooq.VisitContext;
import org.jooq.impl.DSL;
import org.jooq.impl.DefaultVisitListener;

public class VisitListenerInsertFillMetadata extends DefaultVisitListener {

	private static final String INSERT_KEY = "insert";
	private static final String UNDERSCORES = "__";
	private static final Pattern STANDALONE_PATTERN_MATCH_BY = Pattern.compile("^__.*?by$");
	private static final Pattern STANDALONE_PATTERN_MATCH_ON = Pattern.compile("^__.*?on$");

	private static final Pattern ASSOCIATION_PATTERN_MATCH_BY = Pattern.compile(".*?__.*?by$");
	private static final Pattern ASSOCIATION_PATTERN_MATCH_ON = Pattern.compile(".*?__.*?on$");
	private String member;

	public VisitListenerInsertFillMetadata(String member) {
		this.member = member;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public void visitStart(VisitContext context)
	{
		if (context.renderContext() != null) {
			QueryPart part = context.queryPart();

			if(context.data().containsKey(INSERT_KEY) && Clause.TABLE_REFERENCE == context.clause()){
				
				InsertQuery<?> query = (InsertQuery) context.data().get(INSERT_KEY);
				if(part instanceof Table){
					Table<?> insertTable = (Table) part;

					Map<Field<?>, Field<?>> map = getValues(query);//ARGHHH maybe a better usage is to use getParams()?
					//Param<?> p = query.getParams().values().iterator().next();
					
					Field<?>[] fields = insertTable.fields();
					for(Field field : fields){
						if(map.containsKey(field)) {
							//do not override any value if exists
							continue;
						}

						Matcher matcher = STANDALONE_PATTERN_MATCH_BY.matcher(field.getName());
						if (matcher.find()) {
							query.addValue(field, member);
							continue;
						}

						matcher = STANDALONE_PATTERN_MATCH_ON.matcher(field.getName());
						if (matcher.find()) {
							query.addValue((Field<Timestamp>) field, DSL.currentTimestamp());
							continue;
						}


						matcher = ASSOCIATION_PATTERN_MATCH_BY.matcher(field.getName());
						if (matcher.find()) {
							//set value only if baseField is present
							Field dependencyField = getBaseField(insertTable, field);
							if(map.containsKey(dependencyField)){
								query.addValue(field, member);
							}
							continue;
						}

						matcher = ASSOCIATION_PATTERN_MATCH_ON.matcher(field.getName());
						if (matcher.find()){
							//set value only if baseField is present
							Field dependencyField = getBaseField(insertTable, field);
							if(map.containsKey(dependencyField)){
								query.addValue((Field<Timestamp>) field, DSL.currentTimestamp());
							}
							continue;
						}
					}

				}
			}

			if(part instanceof InsertQuery) {
				InsertQuery query = (InsertQuery) part;
				context.data().put(INSERT_KEY, query);
			}
		}
	}

	@SuppressWarnings("rawtypes")
	private Field getBaseField(Table<?> insertTable, Field field) {
		String name = field.getName();
		if (!name.contains(UNDERSCORES)) {
			throw new IllegalArgumentException("Cannot getBaseField() on not metadata field. Actual field '"+name+"'");
		}
		String baseColumnName = name.substring(0, name.lastIndexOf(UNDERSCORES));
		return insertTable.field(baseColumnName);
	}

	@SuppressWarnings("unchecked")
	private Map<Field<?>, Field<?>> getValues(InsertQuery<?> query)
	{
		try {
			Method m = query.getClass().getSuperclass().getDeclaredMethod("getValues");
			m.setAccessible(true);
			return (Map<Field<?>, Field<?>>) m.invoke(query);
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
}
