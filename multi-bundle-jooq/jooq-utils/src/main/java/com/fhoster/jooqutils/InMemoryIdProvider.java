package com.fhoster.jooqutils;

import java.math.BigInteger;

public class InMemoryIdProvider implements IdProvider {

	private BigInteger base = BigInteger.ZERO;
	
	@Override
	public BigInteger get() {
		return increaseAndGet();
	}
	
	private BigInteger increaseAndGet() {
		base = base.add(BigInteger.ONE);
		return base;
	}
}
