package com.fhoster.jooqutils;

import java.math.BigInteger;
import java.sql.Connection;

import javax.sql.DataSource;

import org.jooq.Configuration;
import org.jooq.DSLContext;
import org.jooq.ExecuteListener;
import org.jooq.Field;
import org.jooq.SQLDialect;
import org.jooq.VisitListener;
import org.jooq.VisitListenerProvider;
import org.jooq.conf.Settings;
import org.jooq.impl.DSL;
import org.jooq.impl.DefaultConfiguration;

public class JooqUtils {
	private static SQLDialect dialect = SQLDialect.MARIADB;
	public final static String ID_TEMP = "idTemp";

	public static DSLContext create(Connection c) {
		return DSL.using(c, dialect, getSettings());
	}

	public static DSLContext create(Connection c, VisitListenerInsertFillID idFiller,
			VisitListenerInsertFillMetadata metadataFiller) {
		Configuration config = new DefaultConfiguration().set(c)//
				.set(dialect)//
				.set(getSettings()).set(new VisitListener[] { idFiller, metadataFiller })
				.set(new ExecuteListener[] { idFiller });

		return DSL.using(config);
	}

	public static DSLContext create(Connection c, VisitListener... listeners) {
		Configuration config = new DefaultConfiguration().set(c)//
				.set(dialect)//
				.set(getSettings()).set(listeners);

		return DSL.using(config);
	}

	public static DSLContext create(Connection c, VisitListenerProvider... providers) {
		Configuration config = new DefaultConfiguration().set(c)//
				.set(dialect)//
				.set(getSettings()).set(providers);

		return DSL.using(config);
	}

	private static Settings getSettings() {
		Settings s = new Settings();
		s.setFetchWarnings(false);
		s.setRenderSchema(false);
		s.setExecuteLogging(true);
		return s;
	}

	public static DSLContext create(DataSource ds) {
		return DSL.using(ds, dialect, getSettings());
	}

	public static Field<BigInteger> getIdTempCounter(Connection conn) {
		return getIdTempCounter(create(conn));
	}

	public static Field<BigInteger> getIdTempCounter(DSLContext dsl) {
		return DSL.field("@" + ID_TEMP + ":=@" + ID_TEMP + " + 1", BigInteger.class).as(ID_TEMP);
	}

	public static int setIdTempCounterStartValue(DSLContext dsl, BigInteger bi) {
		return dsl.execute("SET @" + ID_TEMP + " := " + bi.intValueExact() + ";");
	}

	public static int setIdTempCounterStartValue(Connection conn, BigInteger bi) {
		return setIdTempCounterStartValue(create(conn), bi);
	}

	public static Integer getIdTempCounterValue(Connection conn) {
		return getIdTempCounterValue(create(conn));
	}

	public static Integer getIdTempCounterValue(DSLContext dsl) {
		Object o = dsl.fetch("SELECT @" + ID_TEMP + ";").get(0).getValue(0);
		if (null == o)
			return null;
		return Integer.parseInt(o.toString());
	}
}