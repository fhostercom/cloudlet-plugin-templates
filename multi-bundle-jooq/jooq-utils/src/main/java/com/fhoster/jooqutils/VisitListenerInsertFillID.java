package com.fhoster.jooqutils;

import java.lang.reflect.Method;
import java.math.BigInteger;
import java.sql.PreparedStatement;
import java.util.Map;

import org.jooq.Clause;
import org.jooq.ExecuteContext;
import org.jooq.ExecuteListener;
import org.jooq.Field;
import org.jooq.InsertQuery;
import org.jooq.QueryPart;
import org.jooq.VisitContext;
import org.jooq.VisitListener;
import org.jooq.impl.TableImpl;

public class VisitListenerInsertFillID implements VisitListener, ExecuteListener {
	private static final long serialVersionUID = 8189236137706283834L;
	
	private static final String INSERT_KEY = "insert";
	private static final String __ID = "__id";
	
	private IdProvider idProvider;
	private boolean autofilledID = false;

	public VisitListenerInsertFillID(IdProvider idProvider) {
		this.idProvider = idProvider;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public void visitStart(VisitContext context)
	{
		if (context.renderContext() != null) {
			QueryPart part = context.queryPart();

			if(context.data().containsKey(INSERT_KEY) && context.clause()==Clause.TABLE_REFERENCE){
				InsertQuery<?> query = (InsertQuery) context.data().get(INSERT_KEY);
				if(part instanceof TableImpl){
					TableImpl<?> insertTable = (TableImpl) part;

					Map<Field<?>, Field<?>> map = getValues(query); //ARGHHH maybe a better usage is to use getParams()?
//					Param<?> p = query.getParams().values().iterator().next();
					
					Field<?>[] fields = insertTable.fields();
					for(Field field : fields){
						String name = field.getName();
						if (__ID.equals(name) && !map.containsKey(field)) {
							BigInteger autoid = idProvider.get();
							query.addValue(field, autoid);
							autofilledID = true;
						}
					}
				}
			}

			if(part instanceof InsertQuery) {
				InsertQuery query = (InsertQuery) part;
				context.data().put(INSERT_KEY, query);
			}
		}
	}

	@SuppressWarnings("unchecked")
	private Map<Field<?>, Field<?>> getValues(InsertQuery<?> query)
	{
		try {
			Method m = query.getClass().getSuperclass().getDeclaredMethod("getValues");
			m.setAccessible(true);
			return (Map<Field<?>, Field<?>>) m.invoke(query);
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public void start(ExecuteContext ctx) {}

	@Override
	public void renderStart(ExecuteContext ctx) {}

	@Override
	public void renderEnd(ExecuteContext ctx) {}

	@Override
	public void prepareStart(ExecuteContext ctx) {}

	@Override
	public void prepareEnd(ExecuteContext ctx) {}

	@Override
	public void bindStart(ExecuteContext ctx) {}

	@Override
	public void bindEnd(ExecuteContext ctx) {}

	@Override
	public void executeStart(ExecuteContext ctx) {
		if (autofilledID) {
			PreparedStatement actualStatement = ctx.statement();
			PreparedStatement wrappedStatement = new PreparedStatementWrapperWithAutoFilledKeys(actualStatement);
			ctx.statement(wrappedStatement);
		}
	}

	@Override
	public void executeEnd(ExecuteContext ctx) {}

	@Override
	public void outStart(ExecuteContext ctx) {}

	@Override
	public void outEnd(ExecuteContext ctx) {}

	@Override
	public void fetchStart(ExecuteContext ctx) {}

	@Override
	public void resultStart(ExecuteContext ctx) {}

	@Override
	public void recordStart(ExecuteContext ctx) {}

	@Override
	public void recordEnd(ExecuteContext ctx) {}

	@Override
	public void resultEnd(ExecuteContext ctx) {}

	@Override
	public void fetchEnd(ExecuteContext ctx) {}

	@Override
	public void end(ExecuteContext ctx) {}
	
	@Override
	public void exception(ExecuteContext ctx) {}

	@Override
	public void warning(ExecuteContext ctx) {}

	@Override
	public void clauseStart(VisitContext context) {}

	@Override
	public void clauseEnd(VisitContext context) {}

	@Override
	public void visitEnd(VisitContext context) {}
}