# Cloudlet Plugin Templates

This is a reference for setting up a new Livebase Plugin project.

The aim of the templates is to kickstart the development process by providing pre-configured gradle scripts and folder structures, so that you can move on developing your Plugin code.

There are currently three available templates:

- [**mono-bundle**](https://bitbucket.org/fhostercom/cloudlet-plugin-templates/downloads/mono-bundle.tar.gz): the simplest and tiniest; choose it if you want to build a single plugin to deploy on your Cloudlet, or if you are new to Livebase plugins.
- [**multi-bundle**](https://bitbucket.org/fhostercom/cloudlet-plugin-templates/downloads/multi-bundle.tar.gz): a more flexible template which allows to author a Gradle multi-project and build many different plugins for the same Cloudlet.
- [**multi-bundle-jooq**](https://bitbucket.org/fhostercom/cloudlet-plugin-templates/downloads/multi-bundle-jooq.tar.gz): a variant of the multi-bundle which comes preconfigured with Gradle tasks for setting and using jOOQ with your Cloudlet's database.

Each template is a __Gradle__ project and comes packaged with a __Gradle wrapper__, which allows to run Gradle commands without having to install Gradle on your machine.

Each template also includes the [Cloudlet SPI Annotation Processor](https://bitbucket.org/fhostercom/cloudlet-spi-annotation-processor/), to simplify the plugins' OSGi configuration.

## Setup

Download a template from the list above and extract the `.tar.gz` into a folder.

If you already have your Cloudlet's SPI jar, it should be copied under `lib/cloudlet` in the project's root directory, with the name `spi.jar`. However, you can modify these defaults by editing the properties `SPI_LOCATION` and `SPI_FILENAME` inside `gradle.properties` file. You can also change the project's name by opening `settings.gradle` and editing `rootProject.name = '<YourProjectName>'`.

To include dependencies and external libraries, open `build.gradle` and edit the `dependencies` block:

- For compilation-only dependencies (not required in the bundle at runtime, like the Cloudlet SPI jar), please use the `compileOnly` dependency configuration.
- For dependencies which need to be included in the bundle, please use the `implementation` configuration.
- For test dependencies, please use the `testCompileOnly` configuration.

Any additional external library should be put under `lib/`. To configure multi-projects, see the section below.

When you're done, open a shell and type `./gradlew wrapper` (`gradlew wrapper` if you are on Windows).

If you plan to use the project on Eclipse, first run `./gradlew eclipse` to configure correctly the Eclipse project files (`.project`, `.classpath`, etc..), then import the project.
After that, right click on the project and select `Gradle > Refresh Gradle Project`: all the dependencies declared in the build script will be resolved on the classpath, and you'll be ready to code.

## Building a project

When you're done implementing the SPI interfaces, open a shell and type __`./gradlew build`__. Gradle will compile and test the code, run the Annotation Processor to generate the blueprints, collect the `implementation` libraries and generate an OSGi bundle.

Gradle will collect all the bundles and put them in `output`, for easy access.

The default bundle name is `<ProjectName>-<SpiVersion>`, where `<ProjectName>` resolves to the Gradle project's name and `<SpiVersion>` to the value of the property `SPI_VERSION` in the `gradle.properties` file. However, you can change the bundle's name by editing `jar.archiveName` in `build.gradle`.

The OSGi fat jar will look like this:

```text
archiveName/
├── packages/
│   └──.../
├── OSGI-INF/
│   └── blueprint/
├── META-INF/
│   └── manifest.mf
└── lib/
```

- `OSGI-INF` contains the blueprints generated by the _SPI Annotation Processor_.
- `META-INF` contains the manifest declaring the `implementation` level dependencies.
- `lib` contains the jars of the `implementation` level dependencies.

## Authoring multi-project plugin builds

If you plan to build several plugins to run independently on your Cloudlet, use the __multi-bundle__ template. When unpacked, it shows the following structure with initially two subprojects:

```text
root/
├── lib/
│   └── cloudlet/
├── plugin1/
│   ├── src/
│   │   ├──main..
│   │   └──test..
│   └── build.gradle
├── plugin2/
├── build.gradle
├── settings.gradle
├── gradle.properties
└── pluginConfig.gradle
```

To add a subproject, open `settings.gradle` and add its name to the `include` list, then refresh the project to let Gradle create and update the structure.
Each subproject's name will be changed to `<RootName>-<ProjectName>`.

Conceptually, there are two kinds of subprojects: __plugin__ projects and __utility__ projects.

- Plugins should always contain at least one class implementing a SPI. Gradle will build an OSGi bundle only for those projects. To add a "plugin nature" to a subproject, add the following snippet to its `build gradle`:

```groovy
#!groovy
def processorLib = 'com.fhoster.livebase:cloudlet-spi-annotation-processor:+'
dependencies {
  compileOnly spiLib
  compileOnly "org.slf4j:slf4j-api:${SLF4J_VERSION}"
  compileOnly "org.slf4j:slf4j-log4j12:${SLF4J_VERSION}"

  compileOnly processorLib
  annotationProcessor processorLib
}

sourceSets.main.compileClasspath += configurations.implementation
sourceSets.main.runtimeClasspath += configurations.implementation
apply from:'../pluginConfig.gradle'
```

- Utilities are meant to hold shared code and support libraries. A project can be included into another project by adding the following line in the consumer's `build.gradle` dependency block:

```groovy
#!groovy
dependencies {
  implementation project(":${rootProject.name}-<YourUtilityProjectName>")
}
```

  When building the root project, the utilities will be included in the jars of every bundle.

### Referencing bundles

Bundles may reference other bundled packages: to do this, you should add their names to the `commonImports` and `imports` arrays, which are declared in the `ext` section of the `pluginConfig.gradle` file in the root project and in the `build.gradle` files in each subproject, respectively.

`commonImports` will be included in every subproject, whilst `imports` are just for the subproject in which they are declared.

## The jOOQ template

[jOOQ](https://www.jooq.org/doc/3.10/manual/) is a Java library that simplifies the access to a database from your application's code, by generating proper classes and structures based on your database's schema and offering a DSL to construct queries.

The __multi-bundle-jooq__ template contains an utility project named `jooq` which adds the following Gradle tasks:

- `generateDB`: creates a local replica of your Cloudlet's database based on the `structure.sql` in your SPI.
- `generateJooqClasses` (or its alias `jooq`): connects to the db and reverse engineers its schema, generating Jooq sources.
- `cleanJooqClasses` (or its alias `cleanJooq`): cleans all the Jooq generated sources.
- `dropDB`: drops the DB schema.

To use the code generator, [Docker](https://www.docker.com/) needs to be installed on your machine. First, open a shell in the root directory and run `./create-dbms.sh`. A MariaDB Docker container will be created and its IP and port will be exported in `gradle.properties`.

- If you are on Windows, install [Kitematic](https://kitematic.com/) and run `./create-dbms-win.sh` from a bash shell.

With the container running, put your SPI in `lib/cloudlet` and run `./gradlew generateJooqClasses`. Generated code will be available under `src/main/jooq` and can be used in your project to build statically-typed queries to your Cloudlet DB using jOOQ's DSL.

The template also contains a `jooq-utils` utility project which provides useful functions to use in pair with jOOQ sources.

**IMPORTANT**: When using `generateJooqClasses` to generate jOOQ code based on the Cloudlet's SPI, it will be associated to a database with the same name of the `dbName` property, as reported in the `project.ext` section in the `build.gradle` file of the `jooq` project. By default, this value is different from the Cloudlet's database name, which is `workgroup_<CloudletId>`, and this could lead to authorization errors when trying to execute jOOQ queries. To solve this problem, create a jOOQ Settings object like this:

```java
Settings s = new Settings();
s.setFetchWarnings(false);
s.setRenderSchema(false);
s.setExecuteLogging(true);
```

Then invoke `DSL.using()` by passing it your connection and this Settings object. You will then be able to perform jOOQ queries with the DSL without problems.

## Further readings

Please refer to the [Livebase Docs](https://docs.livebase.com/guide/plugins/dev-cycle/) for further information.
To see your Cloudlet's SPI javadoc, append `/docs/index.html` to your Cloudlet's URL.
