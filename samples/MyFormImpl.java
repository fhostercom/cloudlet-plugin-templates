package plugin;

import org.apache.log4j.Logger;

import com.fhoster.livebase.CloudletEventHandler;
import com.fhoster.livebase.cloudlet.Class1FormActionContext;
import com.fhoster.livebase.cloudlet.CloudletDataSource;
import com.fhoster.livebase.cloudlet.HandlerException;
import com.fhoster.livebase.cloudlet.HandlerResult;
import com.fhoster.livebase.cloudlet.SpiClass1FormActionHandlerHandler4;

@CloudletEventHandler
public class MyFormImpl implements SpiClass1FormActionHandlerHandler4 {
	
	private static Logger logger = Logger.getLogger(MyFormImpl.class);
	
	@Override
	public HandlerResult doAction(Class1FormActionContext context) throws HandlerException {
		String msg = "You pressed a button and it did nothing";
		logger.info(msg);
		return context.result().withMessage(msg).info();
	}
}
