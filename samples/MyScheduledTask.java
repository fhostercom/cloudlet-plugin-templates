package plugin;

import java.math.BigInteger;

import org.apache.log4j.Logger;

import com.fhoster.livebase.CloudletScheduledTask;
import com.fhoster.livebase.cloudlet.CloudletIdGenerator;
import com.fhoster.livebase.cloudlet.SpiCloudletScheduledTask;

@CloudletScheduledTask(defaultCronExpression = "* * * * *")
public class MyScheduledTask implements SpiCloudletScheduledTask {

	private CloudletIdGenerator idGenerator;
	private static Logger logger = Logger.getLogger(MyScheduledTask.class);
	
	public MyScheduledTask(CloudletIdGenerator idGenerator) {
		this.idGenerator = idGenerator;
	}

	@Override
	public void run() {
		BigInteger id = idGenerator.get();
		String msg = "I generate pointless ids every minute. Current id: " + id;
		logger.info(msg);
	}

}
